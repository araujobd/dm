package com.bruno.aula;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Aula1Activity extends AppCompatActivity {

  private Button btnFechar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_aula1);
    setTitle(R.string.aula1);

    btnFechar = (Button) findViewById(R.id.btn_fechar);
  }

  public void fechar(View v) {
    finish();
  }
}
