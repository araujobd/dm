package com.bruno.aula;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

  private RelativeLayout layout;
  private Button btAula1, btAula2, btAula3, btAula4;
  private TextView textView;
  private static StringBuilder teste = new StringBuilder();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    addString("onCreate()");
    configurarTela();
  }

  @Override
  protected void onStart() {
    super.onStart();
    addString("onStart()");
  }

  @Override
  protected void onPause() {
    super.onPause();
    addString("onPause()");
  }

  @Override
  protected void onStop() {
    super.onStop();
    addString("onStop()");
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    addString("onDestroy()");
  }

  @Override
  protected void onRestart() {
    super.onRestart();
    addString("onRestart()");
  }

  @Override
  protected void onResume() {
    super.onResume();
    addString("onResume()");
    textView.setText(teste);
    teste.delete(0,teste.length());
  }

  private void configurarTela() {
    layout = (RelativeLayout) findViewById(R.id.layout_root);
    textView = (TextView) findViewById(R.id.textView);
    btAula1 = (Button) findViewById(R.id.btn_aula1);
    btAula2 = (Button) findViewById(R.id.btn_aula2);
    btAula3 = (Button) findViewById(R.id.btn_aula3);
    btAula4 = (Button) findViewById(R.id.btn_aula4);

    btAula1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(MainActivity.this, Aula1Activity.class));
      }
    });

    btAula2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(MainActivity.this, Aula2Activity.class));
      }
    });

    btAula3.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        mostrarMensagem(R.string.aula3);
      }
    });

    btAula4.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        mostrarMensagem(R.string.aula4);
      }
    });


  }


  public void mostrarMensagem(int string) {
    Snackbar.make(layout, string, Snackbar.LENGTH_SHORT).show();
  }
  public void mostrarMensagem(String string) {
    Snackbar.make(layout, string, Snackbar.LENGTH_SHORT).show();
  }

  private void addString(String t) {
    teste.append(t);
    teste.append("\n");
  }
}
