package com.bruno.aula;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Aula2Activity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_aula2);
    setTitle(R.string.aula2);
  }
}
